# The MCF package in a nutshell

The MCF package provides a fast and easily implementable Python program that allows you to deploy the Modified Causal Forest for your application. The scope of the MCF is to estimate heterogeneous treatment effects for multiple treatment models in a selection-on-observables setting.    

This website documents the package in detail explaining how the programme is structured, which parameters can be specified and how this impacts estimation and running times. 

To get you quickly started, start [here](docs/quick_start.md).  For a thorough discussion read through the [MCF Core Walkthrough](docs/part_i.md). Here, every programme step is briefly outlined giving you a deeper understanding of the programmatic implementation. The [Python API](docs/core_6.md)  provides a comprehensive list of functional inputs. In the [Technical Appendix](docs/techn_app.md), we bundle some more technicalities. For further technical details consult [Lechner (2019)](https://arxiv.org/abs/1812.09487). Section [Practical hints](docs/core_5.md) comprises a list of tips for speeding up the programme. Finally, we provide an example for self-study in section [Tutorials](docs/tutorial_1.md).

The MCF package has been made with <span style="color: #FF7F50;">&#9829;</span> in Sankt Gallen, Switzerland! Support us by getting involved as hinted [here](docs/intro_3.md).     
