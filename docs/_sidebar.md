<!-- docs/_sidebar.md -->

* [Home](/)
* [Quick start](quick_start.md)
* [MCF Core Walkthrough](part_i.md)
* [Python API](core_6.md)
* [Practical hints](core_5.md)
* [Technical Appendix](techn_app.md)
* [Examples](tutorial_1.md)
* [Getting involved](intro_3.md)
